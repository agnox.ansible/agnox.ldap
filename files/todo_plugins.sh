yum -y install fusiondirectory-plugin-systems
yum -y install fusiondirectory-plugin-systems-schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/service-fd.schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/systems-fd-conf.schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/systems-fd.schema

yum -y install fusiondirectory-plugin-mail
yum -y install fusiondirectory-plugin-mail-schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/mail-fd.schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/mail-fd-conf.schema

yum -y install fusiondirectory-plugin-alias
yum -y install fusiondirectory-plugin-alias-schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/alias-fd-conf.schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/alias-fd.schema

yum -y install fusiondirectory-plugin-alias
yum -y install fusiondirectory-plugin-alias-schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/alias-fd-conf.schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/alias-fd.schema

yum -y install fusiondirectory-plugin-ssh
yum -y install fusiondirectory-plugin-ssh-schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/openssh-lpk.schema

yum -y install fusiondirectory-plugin-gpg
yum -y install fusiondirectory-plugin-gpg-schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/gpg-fd.schema
fusiondirectory-insert-schema -i /etc/openldap/schema/fusiondirectory/pgp*
